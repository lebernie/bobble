'use strict';

const {SettingsFrontend} = require('./settings_frontend');

async function a_init() {
    console.debug('new_tab.init()');

    await SettingsFrontend.a_ensure_loaded();
    a_on_settings_loaded();
}

async function a_on_settings_loaded() {

    let newtab_behaviour = await SettingsFrontend.a_newtab_behaviour();
    if (typeof newtab_behaviour == 'undefined') {
        newtab_behaviour = SettingsFrontend.a_default_newtab_behaviour();
    }

    console.debug('new_tab.a_on_settings_loaded() -> newtab_behaviour = ' + 
        newtab_behaviour);

    let url = 'about:blank';
    let focus_address_bar = false;

    if ((newtab_behaviour == 'bobble') ||
        (newtab_behaviour == 'same')) {
        url = '/search/search_page.html';

    } else if (newtab_behaviour == 'blank') {
        url = 'about:blank';
        focus_address_bar = true;

    } else {
        console.error('new_tab: Invalid newtab_behaviour value: ' + newtab_behaviour);
        console.error('  -> defaulting to about:blank');
        url = 'about:blank';
        focus_address_bar = true;

    }

    // Set focus on website
    //   https://bugzilla.mozilla.org/show_bug.cgi?id=1411465
    if (focus_address_bar) {
        await browser.tabs.update({
            url: url,
            loadReplace: true,
        });

    } else {
        let tab = await browser.tabs.getCurrent();
        await browser.tabs.create({
            url: url,
            cookieStoreId: tab.cookieStoreId,
        });
        await browser.tabs.remove(tab.id);
    }
}

window.addEventListener('DOMContentLoaded', a_init);
