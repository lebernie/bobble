'use strict';

const {Expression} = require('./expression');
const {Common} = require('./common');

class Engine {
    constructor(options_) {

        let default_options = {
            action: '#',
            description: '??',
            fields: {},
            default_url: null,
            enabled: true,
            visible: true,
            category: Common.DEFAULT_CATEGORY,
            referer: null,
            open_in_new_tab: false,
        };

        let options = extend(default_options, options_);

        let new_fields = parse_fields(options.fields, options.description);
        options.fields = new_fields;

        this._options = options;
    }

    static from_rawengine(raw_engine) {
        return new Engine({
            action: raw_engine.action,
            description: raw_engine.description,
            fields: raw_engine.fields,
            default_url: raw_engine.default_url,
            enabled: raw_engine.enabled,
            visible: raw_engine.visible,
            category: raw_engine.category,
            referer: raw_engine.referer,
            open_in_new_tab: raw_engine.open_in_new_tab,
        });
    }

    build_url(str) {
        let url = this._options.action;
        let first = true;
        for (let k in this._options.fields) {
            let sep;
            if (first) {
                sep = '?';
                first = false;
            } else {
                sep = '&';
            }

            let value_ = this._options.fields[k];
            let value;

            if (value_ instanceof Expression) {
                value = value_.evaluate(str);

            } else {
                value = value_;
            }

            url += sep + encodeURIComponent(k) + '=' + encodeURIComponent(value);
        }

        return url;
    }

    description() {
        return this._options.description;
    }

    default_url() {
        let url = this._options.default_url;

        if (url === null) {
            url = this._options.action;
        }

        return url;
    }

    is_enabled() {
        return this._options.enabled;
    }

    is_visible() {
        return (this._options.enabled && this._options.visible);
    }

    category() {
        return this._options.category;
    }

    referer() {
        return this._options.referer;
    }

    open_in_new_tab() {
        return this._options.open_in_new_tab;
    }
}

function parse_fields(fields, description) {
    let new_fields = {};

    for (let k in fields) {
        let v = fields[k];

        if (typeof v != 'string') {
            let msg = 'Engine.parse_fields(): field value must be a string (k: ' + k + ', v: ' + v + ')';
            console.error(msg);
            console.error('-> ' + description);
            console.error('Either a plain string, or a string expression beginning by a "=" character. For example: ');
            console.error('  ="toto" || q');
            console.error('q is remplaced by the query string');
            console.error('The operator for string concatenation is: ||');
            throw new Error(msg + '. See error log');
        }

        let value;
        if (v[0] == '=') {
            // parse it as an expression
            value = new Expression(v.substr(1));
        } else {
            // parse it as a simple string
            value = v;
        }

        new_fields[k] = value;
    }

    return new_fields;
}

function extend(a, b) {
    for(var key in b)
        if(b.hasOwnProperty(key))
            a[key] = b[key];
    return a;
}

exports.Engine = Engine;
