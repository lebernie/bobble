'use strict';

class Terminator {
    constructor(word, description) {
        this._word = word;
        this._description = description;
    }

    word() {
        return this._word;
    }

    description() {
        return this._description;
    }
}

class Alternatives {
    constructor() {
        this._root = new Map();
    }

    add_word(word, description) {
        if (word.length == 0) {
            return;
        }

        this._new_letter(word[0], word.substr(1), description);
    }

    dump(d=null, indent='') {
        if (d === null) {
            d = this._root;
        }

        for (let [k, v] of d) {
            if (v instanceof Terminator) {
                console.info('[%s] %s', k, v.description());
            } else {
                console.info('%s%s', indent, k);
                this.dump(v, indent + '  ');
            }
        }
    }

    alternatives(prefix) {
        let d = this._root;

        for (let l of prefix) {
            d = d.get(l) || null;
            if (d === null) {
                break;
            }
        }

        let terminators = this._get_terminators(d);

        return terminators;
    }

    _new_letter(letter, rem, description, d=null, acc='') {
        if (d === null) {
            d = this._root;
        }

        // console.error('letter: %s rem: %s, d: %o, acc: %s',
        //     letter, rem, d, acc);

        if (! (d.has(letter))) {
        // if (! letter in d) {
            d.set(letter, new Map());
        }

        let new_d = d.get(letter);
        let new_acc = acc + letter;

        if (rem.length != 0) {
            this._new_letter(rem[0], rem.substr(1), description, new_d, new_acc);
        } else {
            new_d.set(new_acc, new Terminator(new_acc, description));
        }
    }

    _get_terminators(d, acc=null) {
        if (d === null) {
            return [];
        }

        if (acc === null) {
            acc = [];
        }

        // console.debug(d);
        for (let [k, v] of d.entries()) {
            // console.debug('_get_terminators(): k: %o, v: %o', k, v);
            if (v instanceof Terminator) {
                acc.push([k, v.description()]);

            } else {
                this._get_terminators(v, acc);
            }
        }

        return acc;
    }
}

exports.Alternatives = Alternatives;

// var a = new Alternatives();
// a.add_word('pastèque', 'description 1')
// a.add_word('pascal', 'description 2')
// a.add_word('pastis', 'description 3')
// a.add_word('mot', 'description 4')
// a.add_word('motard', 'description 5')
// 
// // a.dump()
// 
// console.log(a.alternatives('pantalon'))
// console.log(a.alternatives('past'))
// console.log(a.alternatives('mo'))

