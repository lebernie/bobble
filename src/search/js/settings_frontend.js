'use strict';

const {Engine} = require('./engine');

const PORT_NAME = 'port-background';

let SettingsFrontend = {
    a_backgound_reload: async () => {
        return await a_generic_command('background.reload');
    },

    a_build_time: async () => {
        return await a_generic_command('settings.build_time');
    },

    a_default_newtab_behaviour: async () => {
        return await a_generic_command('settings.default_newtab_behaviour');
    },

    a_ensure_loaded: async () => {
        return await a_generic_command('settings.ensure_loaded');
    },

    a_force_load: async () => {
        return await a_generic_command('settings.force_load');
    },

    a_save: async () => {
        return await a_generic_command('settings.save');
    },

    a_clear: async () => {
        return await a_generic_command('settings.clear');
    },

    a_export_settings: async () => {
        return await a_generic_command('settings.export_settings');
    },

    a_import_settings: async settings_str => {
        return await a_generic_command('settings.import_settings', [settings_str]);
    },

    a_engines: async () => {
        let raw_engines = await a_generic_command('settings.raw_engines');
        let raw_engines2 = raw_engines.map(([shortcut, raw_engine]) => {
            return [shortcut, Engine.from_rawengine(raw_engine)];
        });

        return new Map(raw_engines2);
    },

    a_default_shortcut: async () => {
        return await a_generic_command('settings.default_shortcut');
    },

    a_newtab_behaviour: async () => {
        return await a_generic_command('settings.newtab_behaviour');
    },

    a_default_category_name: async () => {
        return await a_generic_command('settings.default_category_name');
    },

    a_shortcut_alternatives: async prefix => {
        return await a_generic_command('settings.shortcut_alternatives', [prefix]);
    },

    a_headers_set_referer: async referer => {
        return await a_generic_command('headers.set_referer', [referer]);
    },
};

async function a_generic_command(func, params=[]) {
    return new Promise((resolve, reject) => {
        try {
            let port = browser.runtime.connect({ name: PORT_NAME });

            port.postMessage({
                func: func,
                params: params,
            });

            port.onMessage.addListener(message => {
                let [result, error] = message;

                if (error === null) {
                    resolve(result);

                } else {
                    reject(error);
                }
            });

        } catch(e) {
            console.error('SettingsFrontend.a_generic_command() error: %o', e);
            reject(e);
        }
    });
}

exports.SettingsFrontend = SettingsFrontend;

