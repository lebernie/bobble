'use strict';

const {Parser} = require('expr-eval');

class Expression {
    constructor(expression) {

        const parser = new Parser({
            operators: {
                concatenate: true,
                conditional: true,

                add: false,
                divide: false,
                factorial: false,
                multiply: false,
                power: false,
                remainder: false,
                subtract: false,
                logical: false,
                comparison: false,
                'in': false
            },
        });

        this._expression = parser.parse(expression);
        this._str_expression = expression;
    }

    evaluate(query_string) {
        let result = this._expression.evaluate({
            q: query_string,
        });
        console.error('toto1: ' + result);

        return result;
    }
}

exports.Expression = Expression;
