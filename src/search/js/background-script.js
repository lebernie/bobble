'use strict';

var {SettingsBackend} = require('./settings_backend.js');
var utils = require('./utils');

const EXTENSION_URL = browser.extension.getURL('search/search_page.html');
var g_referer =  null;

function rewrite_referer(e) {
    if ((g_referer === null) && (e.originUrl != EXTENSION_URL)) {
        return {};
    }

    let found = false;
    for (var header of e.requestHeaders) {
        if (header.name.toLowerCase() === "referer") {
            found = true;
            break;
        }
    }

    if (! found) {
        e.requestHeaders.push({
            name: 'Referer',
            value: g_referer,
        });
    }

    return {requestHeaders: e.requestHeaders};
}

async function a_init() {
    browser.runtime.onConnect.addListener(on_connect);
    browser.webRequest.onBeforeSendHeaders.addListener(
        rewrite_referer,
        {urls: ['<all_urls>']},
        ["blocking", "requestHeaders"]
    );
}

function reload() {
    console.info('background: reload_settings()');
    location.reload(true);
}

function on_connect(port) {
    // console.debug('background-script: connect from %o', port);
    port.onMessage.addListener(async (m) => {
        // console.debug('background-script: message: %o', m);

        let settings = SettingsBackend.get_instance();
        let result = null;
        let error = null;

        try {
            if (m.func == 'background.reload') {
                result = reload();

            } else if (m.func == 'settings.build_time') {
                result = settings.build_time();

            } else if (m.func == 'settings.default_newtab_behaviour') {
                result = settings.default_newtab_behaviour();

            } else if (m.func == 'settings.ensure_loaded') {
                result = await settings.a_ensure_loaded();

            } else if (m.func == 'settings.force_load') {
                result = await settings.a_force_load();

            } else if (m.func == 'settings.save') {
                result = await settings.a_save();

            } else if (m.func == 'settings.clear') {
                result = await settings.a_clear();

            } else if (m.func == 'settings.export_settings') {
                result = await settings.a_export_settings();

            } else if (m.func == 'settings.import_settings') {
                let settings_str = m.params[0] || '';
                result = settings.import_settings(settings_str);

            } else if (m.func == 'settings.raw_engines') {
                result = await settings.a_raw_engines();

            } else if (m.func == 'settings.default_shortcut') {
                result = await settings.a_default_shortcut();

            } else if (m.func == 'settings.newtab_behaviour') {
                result = await settings.a_newtab_behaviour();

            } else if (m.func == 'settings.default_category_name') {
                result = await settings.a_default_category_name();

            } else if (m.func == 'settings.shortcut_alternatives') {
                let prefix = m.params[0] || '';
                result = await settings.a_shortcut_alternatives(prefix);

            } else if (m.func == 'headers.set_referer') {
                let referer = m.params[0] || null;
                g_referer = referer;

            } else {
                console.error('background-script: unknown message: %o', m);
            }

        } catch(e) {
            // Clone the exception;
            error = utils.to_json(e);
        }


        // console.debug('background-script: %o(%o) -> (%o, %o)',
        //      m.func, m.params, result, error);

        port.postMessage([result, error]);
    });

}

a_init();
