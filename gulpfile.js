// https://github.com/gulpjs/gulp/blob/master/docs/recipes/browserify-multiple-destination.md
//
const package_json = require('./package.json');
const gulp = require('gulp');
const gulp_if = require('gulp-if');
const log = require('gulplog');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify-es').default;
const template = require('gulp-template');
const del = require('del');
const path = require('path');

const BUILD_TIME = new Date().getTime();
const GLOBAL_PARAMS = {
    version: package_json.version,
    build_time: BUILD_TIME,
};

log.info('Package version: ' + GLOBAL_PARAMS.version);

const DIST_DIR = path.join(__dirname, 'dist');
const DIST_JS_DIR = path.join(DIST_DIR, 'search', 'js');
const DIST_JSMAPS_DIR = path.join(DIST_DIR, 'maps');

var DEBUG = false;
var DO_MINIFY = false;

function do_browserify(input_entry, output_bundle) {
    return function() {
        let b = browserify({
            debug: true,
            entries: input_entry,
        });

        return b.bundle()
            .pipe(source(output_bundle))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(gulp_if(DO_MINIFY, uglify()))
            .on('error', log.error)
            .pipe(sourcemaps.write('./maps'))
            .pipe(gulp.dest('./dist'));

    };
}

gulp.task('browserify_main', do_browserify('src/search/js/main.js', './search/js/main.bundle.js'));
gulp.task('browserify_options', do_browserify('src/search/js/options.js', './search/js/options.bundle.js'));
gulp.task('browserify_new_tab', do_browserify('src/search/js/new_tab.js', './search/js/new_tab.bundle.js'));
gulp.task('browserify_backgroundscript', do_browserify('src/search/js/background-script.js', './search/js/background-script.bundle.js'));

gulp.task('js', gulp.parallel(
    'browserify_main',
    'browserify_options',
    'browserify_new_tab',
    'browserify_backgroundscript'));

gulp.task('templates1', function() {
    return gulp.src([
        'src/manifest.json',
        'src/build_info.json',
    ])
        .pipe(template(GLOBAL_PARAMS))
        .pipe(gulp.dest('dist'));
});

gulp.task('templates2', function() {
    return gulp.src([
        'src/search/search_page.html',
    ])
        .pipe(template(GLOBAL_PARAMS))
        .pipe(gulp.dest('dist/search'));
});

gulp.task('set-debug', function(cb) {
    DEBUG = true;
    // DO_MINIFY = true;
    cb()
});

function strip(cb) {
    if (! DEBUG) {
        let to_delete = [
            DIST_JSMAPS_DIR,
        ];

        log.info('Stripping...');
        to_delete.forEach(function(e) {
            log.info('  - ' + e);
            del(e);
        });
    }

    cb();
}

gulp.task('strip', gulp.series(strip));

gulp.task('build', gulp.series(gulp.parallel('templates1', 'templates2'), 'js', 'strip'));
gulp.task('debug', gulp.series('set-debug', 'build'));

gulp.task('default', gulp.series('build'));

