1 Sources
=========
All source files are available at :

https://bitbucket.org/lebernie/bobble/

2 Build
=======

 $ npm install

2.1 Standard build
------------------
 $ gulp

2.2 Debug build
---------------
When building with debug option, JavaScript source maps from browserify
are not removed. (-> dist/maps)

 $ gulp debug

3 Lint
======

 $ npm run lint

4 Firefox Minimum required versions
===================================

```
  - chrome_settings_overrides/homepage: 55
  - chrome_url_overrides/newtab: 54
```

5 License
=========

The following Source Code files are subject to the terms of the Mozilla Public
License, v. 2.0. You can obtain one at http://mozilla.org/MPL/2.0/.

```
  - .eslintrc.js
  - .hgignore
  - README.md
  - dist/_locales/en/messages.json
  - dist/_locales/fr/messages.json
  - dist/icons/bobble-48.png
  - dist/search/css/main.css
  - dist/search/css/options.css
  - dist/search/js/__blankfile__
  - dist/search/new_tab.html
  - dist/search/options.html
  - doc/settings_example.yaml
  - gulpfile.js
  - index.js
  - package.json
  - src/build_info.json
  - src/manifest.json
  - src/search/js/alternatives.js
  - src/search/js/background-script.js
  - src/search/js/common.js
  - src/search/js/engine.js
  - src/search/js/expression.js
  - src/search/js/main.js
  - src/search/js/new_tab.js
  - src/search/js/options.js
  - src/search/js/settings_backend.js
  - src/search/js/settings_frontend.js
  - src/search/js/utils.js
  - src/search/search_page.html
```

