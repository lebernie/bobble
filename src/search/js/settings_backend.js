'use strict';

const yaml = require('js-yaml');

const {Alternatives} = require('./alternatives');
const {build_time} = require('../../../dist/build_info.json');
const {Common} = require('./common');

const SETTINGS_VERSION = 1;
const NTB_CHOICES = ['bobble', 'same', 'blank'];
const DEFAULT_NEWTAB_BEHAVIOUR = NTB_CHOICES[0];

// Singleton
// (https://stackoverflow.com/questions/1479319/simplest-cleanest-way-to-implement-singleton-in-javascript)
let SettingsBackend = (function() {
    let instance = null;

    return {
        get_instance: function() {
            if (instance === null) {
                instance = new SettingsClass();
                // Hide the constructor to prevent the returned object
                // to be newed
                instance.constructor = null;
            }

            return instance;
        },
    };

})();

class SettingsClass {
    constructor() {
        this._settings = null;
        this._raw_settings = null;
        this._alternatives = null;
        this._build_time = build_time;
    }

    build_time() {
        return this._build_time;
    }

    default_newtab_behaviour() {
        return DEFAULT_NEWTAB_BEHAVIOUR;
    }

    async a_ensure_loaded() {
        if (this._settings === null) {
            await this._a_load();
        }
    }

    async a_force_load() {
        await this._a_load();
    }

    async a_save() {
        // console.debug('SettingsBackend.a_save(): will save settings:');
        // console.debug(this._settings);

        try {
            await browser.storage.local.set({
                contents: this._raw_settings,

            });

            // console.debug('SettingsBackend.a_save():  saved:');
            // console.debug(this._settings);
        } catch (e) {
            console.error('SettingsBackend.a_save(): Failed to save settings: ' + e);
        }
    }

    async a_clear() {
        // console.debug('SettingsBackend: clear');

        try {
            await browser.storage.local.clear();
            console.info('SettingsBackend cleared');
        } catch (e) {
            console.error('Failed to clear settings: ' + e);
        }
    }

    async a_export_settings() {
        // XXX Do not try to load settings: this function MUST NO FAIL XXX
        // XXX await this.a_ensure_loaded();

        return this._raw_settings;
    }

    import_settings(settings_str) {
        this._raw_settings = settings_str;

        // XXX
        let parsed_settings = decode(settings_str);

        this._settings = parsed_settings;

        this._alternatives = new Alternatives();
        for (let [shortcut, raw_engine] of this._settings.raw_engines) {
            if (raw_engine.visible) {
                this._alternatives.add_word(shortcut, raw_engine.description);
            }
        }

        // console.debug('SettingsBackend.import_settings(): now:');
        // console.debug(this._settings);
        // console.debug(this._raw_settings);
        // console.debug('SettingsBackend.import_settings(): -----');
    }

    async a_raw_engines() {
        await this.a_ensure_loaded();
        return this._settings.raw_engines;
    }

    async a_default_shortcut() {
        await this.a_ensure_loaded();
        return this._settings.default_shortcut;
    }

    async a_newtab_behaviour() {
        await this.a_ensure_loaded();
        return this._settings.newtab_behaviour;
    }

    async a_default_category_name() {
        await this.a_ensure_loaded();
        return this._settings.default_category_name;
    }

    async a_shortcut_alternatives(prefix) {
        await this.a_ensure_loaded();

        return this._alternatives.alternatives(prefix);
    }

    // "Private" functions
    async _a_load() {
        console.debug('Load settings...');
        let raw_settings_ = await browser.storage.local.get(null);
        let raw_settings = raw_settings_.contents;
        try {
            if (typeof raw_settings == 'undefined') {
                console.info('No settings yet => initialize new settings');
                this.import_settings(new_rawsettings());

            } else {
                try {
                    this.import_settings(raw_settings);
                    // console.debug('SettingsBackend._a_load(): decoded: ' + JSON.stringify(this._settings));

                } catch(e) {
                    console.error('SettingsBackend._a_load(): Failed to decode: ' + e);
                    throw e;
                    // this.import_settings(new_rawsettings);
                }
            }
        } catch (e) {
            console.error(e);
            throw e;
        }
    }

}

function new_rawsettings() {
    // console.debug('SettingsBackend.new_rawsettings(): Generating new settings');
    let r = [
        'version: ' + SETTINGS_VERSION,
        'default_shortcut: g',
        'newtab_behaviour: bobble',
        'engines:',
        '    - shortcut: g',
        '      description: Google (fr)',
        '      action: https://www.google.fr/search',
        '      default_url: https://www.google.fr/',
        '      enabled: true',
        '      visible: false',
        '      category: ' + Common.DEFAULT_CATEGORY,
        '      fields:',
        '          q: = q',
        '',
        '    - shortcut: q',
        '      description: Qwant (fr)',
        '      action: https://www.qwant.com/',
        '      default_url: https://www.qwant.com/?l=fr&h=1&hc=1&a=0&s=0&b=0&i=0&r=FR&sr=fr',
        '      enabled: true',
        '      visible: true',
        '      category: Search',
        '      fields:',
        '          q: = q',
        '          l: fr',
        '          h: "1"',
        '          hc: "1"',
        '          a: "0"',
        '          s: "0"',
        '          b: "0"',
        '          i: "0"',
        '          r: FR',
        '          sr: fr',
        '',
        '    - shortcut: so',
        '      description: stackoverflow.com',
        '      action: http://www.stackoverflow.com/search',
        '      default_url: http://www.stackoverflow.com/',
        '      category: Dev.',
        '      fields:',
        '          q: = q',
        '          tab: votes',
        '',
        '    - shortcut: mdn',
        '      description: MDN',
        '      action: https://developer.mozilla.org/fr/search',
        '      category: Dev.',
        '      fields:',
        '          q: = q',
        '',
        '    - shortcut: wp',
        '      description: Wikipedia (en)',
        '      action: https://en.wikipedia.org/w/index.php',
        '      referer: https://en.wikipedia.org/',
        '      fields:',
        '          search: = q',
        '          title: Special:Search',
        '          go: Go',
    ].join('\n');

    return r;
}

function decode(raw_settings) {
    try {
        let s = yaml.safeLoad(raw_settings);

        let newtab_behaviour = get_prop(s, 'newtab_behaviour', NTB_CHOICES[0]);

        if (NTB_CHOICES.indexOf(newtab_behaviour) < 0) {
            throw new Error('newtab_behaviour (' + newtab_behaviour +
                ') must be one of ' + NTB_CHOICES.join(', '));
        }


        let settings = {
            version: get_prop(s, 'version'),
            default_shortcut: get_prop(s, 'default_shortcut'),
            newtab_behaviour: newtab_behaviour,
            default_category_name: get_prop(s, 'default_category_name', null),
            raw_engines: [],
        };

        s.engines.forEach((e, i) => {
            try {
                let [shortcut, raw_engine] = decode_engine(e);
                settings.raw_engines.push([shortcut, raw_engine]);

            } catch(e) {
                console.error('Failed to decode engine #' + i);
                throw e;
            }
        });

        return settings;

    } catch (e) {

        console.error('Failed to decode settings: ' + e.message);
        // console.error(raw_settings);
        throw e;
    }
}

function decode_engine(raw_engine) {
    try {
        let shortcut = get_prop(raw_engine, 'shortcut');

        let raw_engine2 = {
            description: get_prop(raw_engine, 'description', '??'),
            action: get_prop(raw_engine, 'action'),
            fields: get_prop(raw_engine, 'fields'),
            default_url: get_prop(raw_engine, 'default_url', null),
            enabled: get_prop(raw_engine, 'enabled', true),
            visible: get_prop(raw_engine, 'visible', true),
            category: get_prop(raw_engine, 'category', Common.DEFAULT_CATEGORY),
            referer: get_prop(raw_engine, 'referer', null),
            open_in_new_tab: get_prop(raw_engine, 'open_in_new_tab', false),
        };

        return [shortcut, raw_engine2];

    } catch (e) {
        console.error('Failed to decode engine:');
        console.error(raw_engine);
        throw e;
    }
}

function get_prop(obj, key, default_value) {
    let r;

    if (! obj.hasOwnProperty(key)){
        if (typeof default_value == 'undefined') {
            throw new Error('Failed to get property "' + key + '" of "' + obj + '"');
        } else {
            r = default_value;
        }
    } else {
        r = obj[key];
    }

    return r;
}

exports.SettingsBackend = SettingsBackend;

