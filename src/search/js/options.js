'use strict';

const {SettingsFrontend} = require('./settings_frontend');

async function a_clear_options(e) {
    e.preventDefault();

    await SettingsFrontend.a_clear();
    location.reload(true);
}

async function a_save_options(e) {
    e.preventDefault();

    try {
        let sc = document.getElementById('setting_contents');
        await SettingsFrontend.a_import_settings(sc.value);
        await SettingsFrontend.a_save();
        show_error(false);
    } catch(e) {
        let msg = 'Failed to import settings() or save settings';
        console.error(e);
        show_error(msg);
    }
}

async function a_restore_options() {
    try {
        await SettingsFrontend.a_force_load();
        show_error(false);
    } catch (e) {
        let msg = 'Failed to reload settings (see logs): ' + e;
        console.error(e);
        show_error(msg);
    }

    let exported = await SettingsFrontend.a_export_settings();
    let sc = document.getElementById('setting_contents');
    sc.value = exported;
}

function show_error(msg) {
    let em = document.getElementById('error_msg');

    if ((msg === null) || (msg === false)) {
        em.style.visibility = 'hidden';
        em.textContent = '';
    } else {
        console.error('**** Error: ****');
        console.error(msg);
        em.style.visibility = 'visible';
        em.textContent = msg;
    }
}

window.addEventListener('DOMContentLoaded', a_restore_options);
document.getElementById('clear_button')
    .addEventListener('click', a_clear_options);

document.querySelector('form')
    .addEventListener('submit', a_save_options);

