module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "webextensions": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2017
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],

        // XXX 20180311 Temporary
        "no-console": "off",
    }
};
