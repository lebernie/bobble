'use strict';

const {SettingsFrontend} = require('./settings_frontend');
const {quoteattr, escape_html} = require('./utils');
const {build_time} = require('../../../dist/build_info.json');
const {Common} = require('./common');

const SHORTCUT_CHAR = '!';
const RE_QUERY = /\s+/g;

async function a_on_load() {
    // console.debug('document loaded.');
    try {
        let settings_buildtime = await SettingsFrontend.a_build_time();

        if (settings_buildtime != build_time) {
            console.debug('settings.build_time: %o - build_time: %o', settings_buildtime, build_time);
            console.info('main: Settings need to be reloaded');
            await SettingsFrontend.a_backgound_reload();
            location.reload(true);
        }

        await a_on_settings_loaded();

    } catch(e) {
        on_settings_notloaded(e);
    }
}

async function a_on_settings_loaded() {
    // Read engines from settings
    let engines = await SettingsFrontend.a_engines();

    let engine_lists = new Map();
    // // Create DEFAULT_CATEGORY, so that it will be the first one
    // engine_lists.set(Common.DEFAULT_CATEGORY, []);

    for (let [engine_shortcut, engine] of engines) {
        if (! engine.is_visible()) {
            // Don't show this engine
        } else {
            let category = engine.category();

            if (! engine_lists.has(category)) {
                engine_lists.set(category, []);
            }

            let list = engine_lists.get(category);
            list.push([engine_shortcut, engine]);
        }
    }

    let inner_html = [];
    let shortcut_callbacks = {};

    let i = -1;
    for (let [category, engine_list] of engine_lists) {

        let category_html;
        if (category != Common.DEFAULT_CATEGORY) {
            category_html = escape_html(category);

        } else {
            // This is the default category
            let default_category_name = await SettingsFrontend.a_default_category_name();
            let category_name = default_category_name === null ?
                browser.i18n.getMessage('DEFAULT_CATEGORY') : default_category_name;

            category_html =  escape_html(category_name) || '&nbsp;';
        }

        inner_html.push('    <table>');
        inner_html.push('      <tr><th colspan="2">');
        inner_html.push(category_html);
        inner_html.push('      </tr></th>');

        for (let [engine_shortcut, engine] of engine_list) {
            i += 1;
            let shortcut_ = SHORTCUT_CHAR + engine_shortcut;

            let class_ = 'type_engine';
            let shortcut = shortcut_;
            let description = engine.description();
            let str_type = browser.i18n.getMessage('Engine');
            let href = engine.default_url();

            let shortcut_id = 'shortcut_' + i;

            let newtab_attrs = '';
            if (engine.open_in_new_tab()) {
                newtab_attrs = 'target="_blank" rel="noopener noreferrer" ';
            }

            inner_html.push('    <tr>');
            inner_html.push('      <td id="' + quoteattr(shortcut_id) + '" class="' + quoteattr(class_) +
                '" title="' + quoteattr(str_type) + '">' + shortcut + '</td>');
            inner_html.push('      <td>');
            inner_html.push('        <a ' + newtab_attrs + 'href="' + quoteattr(href) + '">' + escape_html(description) + '</a>');
            inner_html.push('      </td>');
            inner_html.push('    </tr>');

            shortcut_callbacks[shortcut_id] = engine_shortcut;
        }
        inner_html.push('    </table>');
    }

    // Avoid UNSAFE_VAR_ASSIGNMENT from Mozilla linter
    // by not using .innerHTML = '...' directly
    const parser = new DOMParser()
    const parsed = parser.parseFromString(inner_html.join('\n'), 'text/html')
    const tag = parsed.getElementsByTagName('body')[0];

    document.getElementById('available_engines')
        .appendChild(tag);

    // Setup shortcuts click
    for (let shortcut_id in shortcut_callbacks) {
        if (shortcut_callbacks.hasOwnProperty(shortcut_id)) {
            let obj = document.getElementById(shortcut_id);
            obj.addEventListener('click', on_shortcut_click_builder(shortcut_callbacks[shortcut_id]));
        }
    }

    document.getElementById('options_link')
        .querySelector('a')
        .addEventListener('click', () => {
            browser.runtime.openOptionsPage();
        });

    document.getElementById('question')
        .addEventListener('keyup', on_keyup);

    let default_shortcut = await SettingsFrontend.a_default_shortcut();
    await a_setup_searchform(default_shortcut, engines);
}

function on_settings_notloaded(e) {
    // Open the option page
    console.error('Failed to load settings');
    console.error(e);
    browser.runtime.openOptionsPage();
}

async function a_setup_searchform(default_shortcut, engines) {

    document.getElementById('search_form').
        addEventListener('submit', async e => {
            e.preventDefault();

            let str = document.getElementById('question').value;

            let engine = null;
            let actual_str = null;

            try {
                // Parse str and isolate !xxx shortcut if needed
                let [engine_shortcut, actual_str] = read_shortcut_and_str();
                if (engine_shortcut !== null) {
                    // Shortcut here
                    engine = engines.get(engine_shortcut);
                    if (typeof engine == 'undefined') {
                        throw new Error('Failed to get engine: "' + engine_shortcut + '"');

                    } else if (! engine.is_enabled()) {

                        throw new Error('Engine "' + engine_shortcut + '" is disabled');
                    }
                } else {
                    // No shortcut => get default engine
                    actual_str = str;
                    engine = engines.get(default_shortcut);
                    if (typeof engine == 'undefined') {
                        throw new Error('Failed to get default engine');
                    }
                }

                let url;
                if (actual_str !== null) {
                    url = engine.build_url(actual_str);

                    // Set custom referer if any or null to ignore
                    await SettingsFrontend.a_headers_set_referer(engine.referer());

                } else {
                    url = engine.default_url();
                }

                let tab = await browser.tabs.getCurrent();

                browser.tabs.update(tab.id, {
                    url: url,
                });

            } catch (e) {

                let msg = e.message;

                console.error(msg);
                console.error(e);
                show_errormessage(msg);

                throw e;
            }

        });
}

function show_errormessage(msg) {
    let msg_box = document.getElementById('error_message');
    msg_box.style.opacity = 1;
    msg_box.textContent = msg;

    setTimeout(() => {
        // FIXME
        // $msg_box.animate({
        //     opacity: 0,
        // });
        msg_box.style.opacity = 0;

    }, 5000);
}

function on_keyup(e) {
    if (!is_printable(e.keyCode)) {
        return;
    }

    (async () => {
        try {
            let [engine_shortcut, s] = read_shortcut_and_str();
            if (engine_shortcut !== null) {
                let datalist = document.getElementById('suggestions');
                while (datalist.firstChild) {
                    datalist.removeChild(datalist.firstChild);
                }

                let alternatives = await SettingsFrontend.a_shortcut_alternatives(engine_shortcut);
                for (let [word, description] of alternatives) {
                    let option = document.createElement('option');
                    option.value = quoteattr(SHORTCUT_CHAR + word + ' ');
                    option.setAttribute('label', SHORTCUT_CHAR + word + ' - ' + description);
                    datalist.appendChild(option);
                }
            }

        } catch(e) {
            console.error('main.on_keyup() error:');
            console.error(e);
            throw e;
        }
    })();
}

function read_shortcut_and_str() {
    let engine_shortcut = null;
    let actual_str = '';

    let s = document.getElementById('question').value;
    let s2 = s.trimLeft();
    if ((s2.length >= 1) && (s2[0] == SHORTCUT_CHAR)) {
        // Shortcut here
        let r = RE_QUERY.exec(s2);

        if (r === null) {
            // No parameters after the shortcut
            engine_shortcut = s2.substr(1);
            actual_str = null;
        } else {
            engine_shortcut = s2.substr(1, r.index).trim();
            actual_str = s2.substr(RE_QUERY.lastIndex);
        }

        // reset RE_QUERY
        RE_QUERY.lastIndex = 0;

    } else {
        engine_shortcut = null;
        actual_str = s2;
    }

    return [engine_shortcut, actual_str]
}

function is_printable(keycode) {
    var valid =
        (keycode > 47 && keycode < 58)   || // number keys
        keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
        (keycode > 64 && keycode < 91)   || // letter keys
        (keycode > 95 && keycode < 112)  || // numpad keys
        (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
        (keycode > 218 && keycode < 223);   // [\]' (in order)

    return valid;
}

function on_shortcut_click_builder(engine_shortcut)
{
    return (e) => {
        let [old_sc, actual_str] = read_shortcut_and_str();
        let question = document.getElementById('question');
        question.value = SHORTCUT_CHAR + engine_shortcut + ' ' + actual_str;
        question.focus();
    }
}

window.addEventListener('DOMContentLoaded', a_on_load);
